import Vue from 'vue';
import VueRouter from 'vue-router';

import Main from '@/views/Main';
import Favorites from '@/views/Favorites';


Vue.use(VueRouter);

const routes = [
  {
    path: '/main/:id',
    name: 'main',
    component: Main,
  },
  {
    path: '/favorites',
    name: 'favorites',
    component: Favorites,
  },
  
];

const router = new VueRouter({
  mode: 'history',
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.name == null) next({ name: "main" });
  else next();
});
export default router;
